package ash_experiments;


import openfl.events.Event;
import openfl.display.Sprite;

/**
 * Main class
 */
class Main extends Sprite {
    public function new() {
        super();
        addEventListener(Event.ENTER_FRAME, onEnterFrame);
    }

    private function onEnterFrame(event:Event):Void {
        removeEventListener(Event.ENTER_FRAME, onEnterFrame);

        var pathTest:PathTest = new PathTest(this);
        pathTest.start();
    }

}
