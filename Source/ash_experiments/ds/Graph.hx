package ash_experiments.ds;

import haxe.ds.ObjectMap;

class Graph<T> {

    private var _outbound:ObjectMap<GraphArc, GraphNode<T>>;
    private var _inbound:ObjectMap<GraphArc, GraphNode<T>>;

    // A multi-key map where source and target nodes are keys and a grapharc instance is the value
    private var srcTargetMap:ObjectMap<GraphNode<T>, ObjectMap<GraphNode<T>, GraphArc>>;

    // Content to node map
    private var contentNodeMap:ObjectMap<T, GraphNode<T>>;

    public function new() {
        srcTargetMap = new ObjectMap<GraphNode<T>, ObjectMap<GraphNode<T>, GraphArc>>();
        contentNodeMap = new ObjectMap<T, GraphNode<T>>();
        _outbound = new ObjectMap<GraphArc, GraphNode<T>>();
        _inbound = new ObjectMap<GraphArc, GraphNode<T>>();
    }

    public function iterator():Iterator<GraphNode<T>> {
        return contentNodeMap.iterator();
    }

    public function contentIterator():Iterator<T> {
        return contentNodeMap.keys();
    }

    public function findNode(data:T):GraphNode<T> {
        return contentNodeMap.get(data);
    }

    public function contains(data:T):Bool {
        return contentNodeMap.exists(data);
    }

    public function getInbound(data:T):Array<T> {
        var list:Array<T> = new Array<T>();
        var iterator:Iterator<GraphArc> = contentNodeMap.get(data).getInboundIterator();

        for (arc in iterator) {
            list.push(_outbound.get(arc).val);
        }

        return list;
    }

    public function getOutbound(data:T):Array<T> {
        var list:Array<T> = new Array<T>();
        var iterator:Iterator<GraphArc> = contentNodeMap.get(data).getOutboundIterator();

        for (arc in iterator) {
            list.push(_inbound.get(arc).val);
        }

        return list;
    }

    public function getOutboundOnly():Array<T> {
        var list:Array<T> = new Array<T>();
        var iterator:Iterator<GraphNode<T>> = contentNodeMap.iterator();

        for (node in iterator) {
            if (node.inDegree == 0 && node.outDegree > 0) {
                list.push(node.val);
            }
        }

        return list;
    }

    public function getInboundOnly():Array<T> {
        var list:Array<T> = new Array<T>();
        var iterator:Iterator<GraphNode<T>> = contentNodeMap.iterator();

        for (node in iterator) {
            if (node.outDegree == 0 && node.inDegree > 0) {
                list.push(node.val);
            }
        }

        return list;
    }

    public function createNode(data:T):GraphNode<T> {
        var node:GraphNode<T> = new GraphNode<T>(this, data);
        contentNodeMap.set(data, node);

        return node;
    }

    public function removeNode(node:GraphNode<T>):T {
        var v:T = node.val;

        contentNodeMap.remove(v);
        unlink(node);
        node.free();

        return v;
    }

    public function addArc(source:GraphNode<T>, target:GraphNode<T>, weight:Float = 1.):GraphArc {
        var arc:GraphArc = null;
		var subMap:ObjectMap<GraphNode<T>, GraphArc>;
		
        if(source.memberOf(this) && target.memberOf(this)) {

			subMap = srcTargetMap.get(source);
			
            if (subMap == null) {
				subMap = new ObjectMap<GraphNode<T>, GraphArc>();
                srcTargetMap.set(source, subMap);
            }

            arc = subMap.get(target);

            if (arc == null) {
                arc = new GraphArc(weight);

                _outbound.set(arc, source);
                _inbound.set(arc, target);

                subMap.set(target, arc);

                source.addOutboundArc(arc);
                target.addInboundArc(arc);
            }

        }

        return arc;
    }

    public function unlink(node:GraphNode<T>):GraphNode<T> {
        var outboundIterator:Iterator<GraphArc> = node.getOutboundIterator();
        var inboundIterator:Iterator<GraphArc> = node.getInboundIterator();

        for (arc in outboundIterator) {
            _outbound.remove(arc);
        }

        for (arc in inboundIterator) {
            _inbound.remove(arc);
        }

        node.clear();

		return node;
    }
}
