package ash_experiments.ds;

class GraphNode<T> {

    private var _inbound:List<GraphArc>;
    private var _outbound:List<GraphArc>;

    public var val:T;

    public var inDegree(get, null): Int;
    public var outDegree(get, null): Int;

    var _graph:Graph<T>;

    public function new(graph:Graph<T>, x:T) {
        val = x;

        _graph = graph;

        _inbound = new List<GraphArc>();
        _outbound = new List<GraphArc>();

    }

    function get_inDegree():Int {
        return _inbound.length;
    }

    function get_outDegree():Int {
        return _outbound.length;
    }

    public function clear():Void {
        _inbound.clear();
        _outbound.clear();
    }

    public function free():Void {
        clear();

        val = null;
        _graph = null;
        _inbound = null;
        _outbound = null;
    }

    public function getOutboundIterator():Iterator<GraphArc> {
        return _outbound.iterator();
    }

    public function getInboundIterator():Iterator<GraphArc> {
        return _inbound.iterator();
    }

    public function addOutboundArc(arc:GraphArc):Void {
        _outbound.add(arc);
    }

    public function addInboundArc(arc:GraphArc):Void {
        _inbound.add(arc);
    }

    public function memberOf(graph:Graph<T>):Bool {
        return (graph == _graph);
    }
}
