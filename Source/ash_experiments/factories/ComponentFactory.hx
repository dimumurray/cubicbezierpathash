package ash_experiments.factories;

import ash_experiments.components.*;
import ash_experiments.components.tags.*;

import ash_experiments.vo.CubicBezierSegment;

import openfl.display.Sprite;

class ComponentFactory {

    public function new() {
    }

    public function createGameState(running:Bool):GameState {
        var gameState:GameState = new GameState();

        gameState.running = running;

        return gameState;
    }

    public function createDisplay(container:Sprite):Display {
        var display:Display = new Display();

        display.displayObject = cast(container.addChild(new Sprite()), Sprite);

        return display;
    }

    public function createTransform(x:Float, y:Float, rotation:Float = 0.0, scale:Float = 1.0):Transform2D {
        var transform:Transform2D = new Transform2D();

        transform.x = x;
        transform.y = y;
        transform.rotation = rotation;
        transform.scale = scale;

        return transform;
    }

    public function createTransformation():Transformation {
        var transformation:Transformation = new Transformation();

        return transformation;
    }

    public function createPathTraversal():PathTraversal {
        var pathTraversal:PathTraversal = new PathTraversal();

        return pathTraversal;
    }

    public function createPathSegment(segment:CubicBezierSegment, step:Float, t:Float):PathSegment {
        var pathSegment:PathSegment = new PathSegment();

        pathSegment.segment = segment;
        pathSegment.step = step;
        pathSegment.t = t;

        return pathSegment;
    }

    public function createSpawnTrackerControl(trigger:Int):SpawnTrackerControl {
        var spawnTrackerControl:SpawnTrackerControl = new SpawnTrackerControl();

        spawnTrackerControl.trigger = trigger;

        return spawnTrackerControl;
    }
}
