package ash_experiments.factories;

import ash_experiments.vo.CubicBezierSegment;

class VOFactory {
    private var segments:Array<CubicBezierSegment>;

    public function new() {
        segments = new Array<CubicBezierSegment>();
    }

    public function createSegment(coords:Array<Float>, resolution:Int = 250):CubicBezierSegment {
        var segment:CubicBezierSegment = new CubicBezierSegment();

        var ax:Float = coords[0];
        var ay:Float = coords[1];

        var bx:Float = coords[2];
        var by:Float = coords[3];

        var cx:Float = coords[4];
        var cy:Float = coords[5];

        var dx:Float = coords[6];
        var dy:Float = coords[7];

        // Calculate coefficients for position equation
        segment.t3x = dx + (3 * (bx - cx)) - ax;
        segment.t3y = dy + (3 * (by - cy)) - ay;

        segment.t2x = 3 * (ax - (2 * bx) + cx);
        segment.t2y = 3 * (ay - (2 * by) + cy);

        segment.t1x = 3 * (bx - ax);
        segment.t1y = 3 * (by - ay);

        segment.t0x = ax; // Note: This pair of values is also the start anchor point
        segment.t0y = ay;

        segment.tNx = dx; // Note: This pair of values is also the end anchor point
        segment.tNy = dy;

        // Calculate coefficients for tangent vector
        segment.tt2x = dx + (3 * (bx - cx)) - ax;
        segment.tt2y = dy + (3 * (by - cy)) - ay;

        segment.tt1x = 2 * (ax - (2 * bx) + cx);
        segment.tt1y = 2 * (ay - (2 * by) + cy);

        segment.tt0x = bx - ax; // Note: This pair of values is also the direction vector from the start anchor point
        segment.tt0y = by - ay;

        // Arc length calculations.
        segment.resolution = resolution;

        segment.subArcLengths = new Array<Float>();
        segment.subArcLengths.push(0);

        var i:Int = 0,
            length:Float = 0,
            x0:Float = ax, y0:Float = ay,
            x1:Float, y1:Float,
            _t:Float;

        while(i < resolution) {
            _t = (i+1)/resolution;

            x1 = ax + _t * (segment.t1x + _t * (segment.t2x + _t * segment.t3x));
            y1 = ay + _t * (segment.t1y + _t * (segment.t2y + _t * segment.t3y));

            length += Math.sqrt((x1 - x0) * (x1 - x0) + (y1 - y0) * (y1 - y0));

            segment.subArcLengths.push(length);

            x0 = x1;
            y0 = y1;

            i++;
        }

        segment.length = length;

        segment.instanceID = segments.length;
        segments.push(segment);

        return segment;
    }

    public function getSegmentByID(id:Int):CubicBezierSegment {
        return segments[id];
    }

}
