package ash_experiments.factories;

import openfl.display.Sprite;
import openfl.display.Graphics;

import openfl.ui.Keyboard;

import ash.core.Entity;
import ash.core.Engine;

import ash_experiments.components.*;
import ash_experiments.components.tags.*;

import ash_experiments.vo.CubicBezierSegment;

import ash_experiments.factories.ComponentFactory;

class EntityFactory {
	private var engine:Engine;
	private var componentFactory:ComponentFactory;
	private var container:Sprite;

	public function new(container:Sprite, engine:Engine, componentFactory:ComponentFactory) {
		this.container = container;
		this.engine = engine;
		this.componentFactory = componentFactory;
	}

	public function createGame():Entity {
		var game:Entity = new Entity();

		// Create game components
		game.add(componentFactory.createGameState(true));
		game.add(componentFactory.createSpawnTrackerControl(Keyboard.SPACE));

        engine.addEntity(game);

		return game;
	}

    /**
     * Creates a tracker Entity;
     *
     * @param segment Cubic Bezier Segment
     * @param step  Velocity of tracker entity.
	 * TODO: Add state argument and refactor for serialization
     */
	public function createTracker(segment:CubicBezierSegment, step:Float = 0, t:Float = 0):Entity {
		var entity:Entity = new Entity();

		// Create Transform Component
		var transform:Transform2D = componentFactory.createTransform(
				segment.t0x + t * (segment.t1x + t * (segment.t2x + t * segment.t3x)),
				segment.t0y + t * (segment.t1y + t * (segment.t2y + t * segment.t3y)),
				Math.atan2(
						segment.tt0y + t * (segment.tt1y + t * segment.tt0y),
						segment.tt0x + t * (segment.tt1x + t * segment.tt0x)
					)
			);

		// Create Display Component
		var display:Display = componentFactory.createDisplay(container);
		var graphics:Graphics = display.displayObject.graphics;

		display.displayObject.x = transform.x;
		display.displayObject.y = transform.y;
		display.displayObject.rotation = (transform.rotation / Math.PI) * 180;

		graphics.beginFill(0x0066CC);
		graphics.drawRect(-8, -8, 16, 16);
		graphics.endFill();

		graphics.lineStyle(2, 0xF88011);
		graphics.moveTo(0, -32);
		graphics.lineTo(0, -16);
		graphics.moveTo(0, 16);
		graphics.lineTo(0, 32);

		// Create PathSegment Component
		var pathSegment:PathSegment = componentFactory.createPathSegment(segment, step, t);

		// Create Tag Components
		var transformation:Transformation = componentFactory.createTransformation();
		var pathTraversal:PathTraversal = componentFactory.createPathTraversal();

		// Add components to tracker entity
		entity.add(display);
	    entity.add(transform);
		entity.add(pathSegment);
		entity.add(pathTraversal);
		entity.add(transformation);

		engine.addEntity(entity);

		return entity;
	}

	public function destroyEntity(entity:Entity):Void {
		engine.removeEntity(entity);
	}
}
