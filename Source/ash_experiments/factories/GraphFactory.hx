package ash_experiments.factories;

import ash_experiments.vo.CubicBezierSegment;

import ash_experiments.ds.Graph;
import ash_experiments.ds.GraphNode;

import Std.parseFloat;


/**



 */

class GraphFactory {
    /**
     * Creates a graph from a SVG file.
     *
     * @param svg Path to the SVG file
     * @param createSegment Delegate function used to create a segment
     *
     * @return A Graph of CubicBezierSegments
     **/
    static public function getGraphFromSVG(svg:String, createSegment:Array<Float>->Int->CubicBezierSegment):Graph<CubicBezierSegment> {
        var graph:Graph<CubicBezierSegment> = new Graph<CubicBezierSegment>();
        var paths:Iterator<Xml> = Xml.parse(svg).firstElement().elementsNamed("path");
        var newGraphNode:GraphNode<CubicBezierSegment>;
        var newSegment:CubicBezierSegment;
        var segment:CubicBezierSegment;

        for (path in paths) {
            newSegment = createSegment(
                parseCoordData(path.get("d")),
                250
            );

            newGraphNode = graph.createNode(newSegment);

            for(node in graph) {
                segment = node.val;

                if (isContinuous(segment, newSegment)) {
                    graph.addArc(node, newGraphNode, node.val.length);
                }

                if (isContinuous(newSegment, segment)) {
                    graph.addArc(newGraphNode, node, newGraphNode.val.length);
                }
            }
        }

        return graph;

    }

    static private function parseCoordData(data:String):Array<Float> {
        //var _pattern:EReg = ~/M(\-?[0-9]+(?=.[0-9]+)? \-?[0-9]+(?=.[0-9]+)?)C(\-?[0-9]+(?=.[0-9]+)? \-?[0-9]+(?=.[0-9]+)?) (\-?[0-9]+(?=.[0-9]+)? \-?[0-9]+(?=.[0-9]+)?) (\-?[0-9]+(?=.[0-9]+)? \-?[0-9]+(?=.[0-9]+)?)/ig;
        var _pattern:EReg = ~/M(\-?[0-9]+ \-?[0-9]+)C(\-?[0-9]+ \-?[0-9]+) (\-?[0-9]+ \-?[0-9]+) (\-?[0-9]+ \-?[0-9]+)/ig;
        var coords:Array<Float> = new Array<Float>();
		var match:String;
		var tokens:Array<String>;
		
        _pattern.match(data);

        for (i in 1...5) {
			match = _pattern.matched(i);
			tokens = match.split(" ");
			
			while (tokens.length > 0) {
				coords.push(parseFloat(tokens[0]));
				tokens.shift();
			}

        }

        return coords;
    }

    /**
     * Test for first-order parametric continuity of curve segments "a" and "b"
     * by:
     *  - Determining if the segments meet end-to-start
     *  - Comparing the slopes of their tangential vectors
     *
     * Tangential vectors are calculated as follows, where t of "a" is 1
     * and t of "b" is 0 for the equations:
     *
     *      tangentX = segment.tt0x + t * (segment.tt1x + t * segment.tt2x);
     *      tangentY = segment.tt0y + t * (segment.tt1y + t * segment.tt2y);
     *
     * And slope is calculated as:
     *      slope = tangentY/tangentX;
     **/
    static private function isContinuous(a:CubicBezierSegment, b:CubicBezierSegment):Bool {
        var result:Bool = false,
            slopeA:Float,
            slopeB:Float;

        if (a.tNx == b.t0x && a.tNy == b.t0y) {
            slopeA = (a.tt0y + a.tt1y + a.tt2y)/(a.tt0x + a.tt1x + a.tt2x);
            slopeB = b.tt0y / b.tt0x;

            result = (slopeA == slopeB);
        }

        return result;
    }
}