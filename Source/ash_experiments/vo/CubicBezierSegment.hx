package ash_experiments.vo;

/**



 */
class CubicBezierSegment {
    // Instance id
    public var instanceID:Int;

    // coefficients for position equation
    public var t3x:Float;
    public var t3y:Float;

    public var t2x:Float;
    public var t2y:Float;

    public var t1x:Float;
    public var t1y:Float;

    public var t0x:Float;
    public var t0y:Float;

    public var tNx:Float;
    public var tNy:Float;

    // coefficients for tangent vector equation
    public var tt2x:Float;
    public var tt2y:Float;

    public var tt1x:Float;
    public var tt1y:Float;

    public var tt0x:Float;
    public var tt0y:Float;

    // Number of sample points along the curve
    public var resolution:Int;

    // Sub Arc lengths
    public var subArcLengths:Array<Float>;

    // Arc length
    public var length:Float;
	
	public function new() {}
}
