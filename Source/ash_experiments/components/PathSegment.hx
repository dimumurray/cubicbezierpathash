package ash_experiments.components;

import ash_experiments.vo.CubicBezierSegment;

class PathSegment {
    public var segment:CubicBezierSegment = null;
    public var step:Float = 0;
    public var t:Float = 0;
	
	public function new(){}
}
