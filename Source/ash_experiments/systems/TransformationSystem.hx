package ash_experiments.systems;

import openfl.display.Sprite;
import Std.int;

import ash.tools.ListIteratingSystem;

import ash_experiments.nodes.TransformationNode;
import ash_experiments.components.Display;
import ash_experiments.components.Transform2D;

class TransformationSystem extends ListIteratingSystem<TransformationNode> {

    public function new() {
        super(TransformationNode, updateNode);
    }

    public function updateNode(node:TransformationNode, time:Float):Void {
        var display:Display = node.display;
        var transform:Transform2D = node.transform;

        display.displayObject.x = transform.x;
        display.displayObject.y = transform.y;
        display.displayObject.rotation = transform.rotation;
    }
}
