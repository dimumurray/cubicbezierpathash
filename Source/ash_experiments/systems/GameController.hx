package ash_experiments.systems;

import ash.core.Engine;
import ash.core.System;
import ash.core.NodeList;
import ash_experiments.ds.Graph;

import ash_experiments.vo.CubicBezierSegment;
import ash_experiments.components.SpawnTrackerControl;

import ash_experiments.factories.EntityFactory;
import ash_experiments.factories.ComponentFactory;

import ash_experiments.nodes.GameNode;

import net.richardlord.input.KeyPoll;

class GameController extends System {
    private var entityFactory:EntityFactory;
    private var graph:Graph<CubicBezierSegment>;
    private var gameNodes:NodeList<GameNode>;

    private var keyPoll:KeyPoll;

    public function new(entityFactory:EntityFactory, graph:Graph<CubicBezierSegment>, keyPoll:KeyPoll) {
        super();
        this.entityFactory = entityFactory;
        this.graph = graph;
        this.keyPoll = keyPoll;
    }

    override public function addToEngine(engine:Engine):Void {
        gameNodes = engine.getNodeList(GameNode);
    }

    override public function update(time:Float):Void {
        for (node in gameNodes) {
            var running:Bool = node.state.running;
            var control:SpawnTrackerControl = node.spawnTrackerControl;
            var v:Float = Math.floor(Math.random() * 2);

            if (running) {
                if (keyPoll.isDown(control.trigger) && control.isDown == false ) {
                    var segments:Array<CubicBezierSegment> = graph.getOutboundOnly();

                    entityFactory.createTracker(
                            segments[Math.floor(Math.random() * segments.length)],
                            0.5 + (Math.random() * 5)
                        );

                    control.isDown = true;
                }

                if (keyPoll.isUp(control.trigger) && control.isDown) {
                    control.isDown = false;
                }
            }
        }
    }

    override public function removeFromEngine(engine:Engine):Void {
        gameNodes = null;
    }
}
