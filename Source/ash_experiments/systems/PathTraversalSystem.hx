package ash_experiments.systems;

import Std.int;

import ash.tools.ListIteratingSystem;

import ash_experiments.nodes.PathTraversalNode;
import ash_experiments.components.PathSegment;
import ash_experiments.components.Transform2D;
import ash_experiments.vo.CubicBezierSegment;
import ash_experiments.ds.Graph;

/**



 */
class PathTraversalSystem extends ListIteratingSystem<PathTraversalNode> {
	private var graph:Graph<CubicBezierSegment>;

	public function new(graph:Graph<CubicBezierSegment>) {
		super(PathTraversalNode, updateNode);
		this.graph = graph;
	}


	private function updateNode(node:PathTraversalNode, time:Float):Void {
		var t:Float,
			tangentX:Float,
			tangentY:Float,
			transform:Transform2D = node.transform,
		    pathSegment:PathSegment = node.pathSegment,
		    segment:CubicBezierSegment = pathSegment.segment;

		pathSegment.t += pathSegment.step;

		if ((pathSegment.t/segment.length) > 1) {
			// select the next segment
			var segments:Array<CubicBezierSegment> = graph.getOutbound(segment);

            if (segments.length == 0) {
                segments = graph.getOutboundOnly();
            }

			var index:Int = int(Math.floor(Math.random() * segments.length));

			pathSegment.t = (pathSegment.t - segment.length)/segments[index].length;
			pathSegment.segment = segment = segments[index];
		}

		t = map(pathSegment.t, segment.subArcLengths);

		// point on the segment at t
  		transform.x = segment.t0x + t * (segment.t1x + t * (segment.t2x + t * segment.t3x));
  		transform.y = segment.t0y + t * (segment.t1y + t * (segment.t2y + t * segment.t3y));

		// tangent vector at t
		tangentX = segment.tt0x + t * (segment.tt1x + t * segment.tt2x);
		tangentY = segment.tt0y + t * (segment.tt1y + t * segment.tt2y);

		transform.rotation = (Math.atan2(tangentY, tangentX) * 180)/ Math.PI;
        // TODO: Calculate magnitude for normalize function

	}

	private function map(t:Float, arcLengths:Array<Float>):Float {
		var length:Int = arcLengths.length,
			low:Int = 0,
			high:Int = length,
			i:Int = 0,
			lengthBefore:Float;

		while (low < high) {
			i = low + int((high - low) / 2);

			if (arcLengths[i] < t) {
				low = i + 1;
			} else {
				high = i;
			}
		}

		if (arcLengths[i] > t) {
			i--;
		}

		lengthBefore = arcLengths[i];
		
		return (lengthBefore == t)? (i/length) : (i + (t - lengthBefore)/(arcLengths[i + 1] - lengthBefore))/length;
	}
}
