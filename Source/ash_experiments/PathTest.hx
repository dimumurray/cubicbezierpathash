package ash_experiments;

import openfl.display.Sprite;
import openfl.Assets;

import format.SVG;

import ash.tick.ITickProvider;
import ash.tick.FrameTickProvider;
import ash.core.Engine;

import ash_experiments.systems.GameController;
import ash_experiments.systems.ProximityResolverSystem;
import ash_experiments.systems.PathTraversalSystem;
import ash_experiments.systems.TransformationSystem;

import ash_experiments.factories.EntityFactory;
import ash_experiments.factories.GraphFactory;
import ash_experiments.factories.ComponentFactory;
import ash_experiments.factories.VOFactory;

import ash_experiments.vo.CubicBezierSegment;

import ash_experiments.ds.Graph;
import net.richardlord.input.KeyPoll;
import net.richardlord.enums.SystemPriorities;

class PathTest {

    private var container:Sprite;
    private var engine:Engine;
    private var entityFactory:EntityFactory;
    private var graph:Graph<CubicBezierSegment>;
    private var componentFactory:ComponentFactory;
    private var keyPoll:KeyPoll;
    private var voFactory:VOFactory;
    private var svgPath:String = "Assets/map_00_optimised.svg";
	private var tickProvider:FrameTickProvider;
	
    public function new(container:Sprite) {
        this.container = container;
        init();
    }

    private function init() {
        var svgText:String = Assets.getText(svgPath);
        var svgAsset:SVG = new SVG(svgText);
        svgAsset.render(container.graphics);

        voFactory = new VOFactory();
        engine = new Engine();
        keyPoll = new KeyPoll(container.stage);
        componentFactory = new ComponentFactory();
        entityFactory = new EntityFactory(container, engine, componentFactory);
        graph = GraphFactory.getGraphFromSVG(svgText, voFactory.createSegment);

        engine.addSystem(new GameController(entityFactory, graph, keyPoll), SystemPriorities.preUpdate);
        engine.addSystem(new ProximityResolverSystem(), SystemPriorities.update);
        engine.addSystem(new PathTraversalSystem(graph), SystemPriorities.move);
        engine.addSystem(new TransformationSystem(), SystemPriorities.render);

        entityFactory.createGame();
    }

    public function start() {
        tickProvider = new FrameTickProvider(container);
        tickProvider.add(engine.update);
        tickProvider.start();
    }
}
