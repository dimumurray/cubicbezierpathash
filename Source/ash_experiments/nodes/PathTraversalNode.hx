package ash_experiments.nodes;

import ash.core.Node;

import ash_experiments.components.PathSegment;
import ash_experiments.components.Transform2D;

import ash_experiments.components.tags.PathTraversal;

class PathTraversalNode extends Node<PathTraversalNode> {
    public var pathTraversal:PathTraversal;
    public var pathSegment:PathSegment;
    public var transform:Transform2D;
	
	public function new() {}
}
