package ash_experiments.nodes;

import ash.core.Node;

import ash_experiments.components.Display;
import ash_experiments.components.Transform2D;

import ash_experiments.components.tags.Transformation;

class TransformationNode extends Node<TransformationNode> {
    public var transformation:Transformation;
    public var display:Display;
    public var transform:Transform2D;
	
	public function new() {}
}
