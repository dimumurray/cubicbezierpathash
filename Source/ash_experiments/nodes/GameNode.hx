package ash_experiments.nodes;

import ash.core.Node;

import ash_experiments.components.GameState;
import ash_experiments.components.SpawnTrackerControl;

class GameNode extends Node<GameNode> {
    public var state:GameState;
    public var spawnTrackerControl:SpawnTrackerControl;
	
	public function new(){}
}
